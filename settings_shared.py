DATA_DIR = ""

from sqlalchemy import create_engine

MANAGEMENT_DB_ENGINE = create_engine('postgresql+psycopg2://@/postgres')

def create_engine_for(user, password, database, echo=False):
    from sqlalchemy import create_engine
    return create_engine(
        'postgresql+psycopg2://{}:{}@localhost/{}'.format(user, password, database), echo=echo
    )

ENGINE = create_engine('postgresql+psycopg2://@/db_oceny', echo=False)
"""
    Database that contains mamagement infromation, like grades.
"""
ZAJ_1_ENGINE = create_engine('postgresql+psycopg2://@/zaj1')
ZAJ_2_ENGINE = create_engine('postgresql+psycopg2://@/zaj2')
ZAJ_3_TEST_ENGINE = create_engine('postgresql+psycopg2://@/zaj3_test')

del create_engine

HMAC_KEY = b"""
0}"^jpyK;qJ_A3la(1T:PHDIL[?Fm&Qy[BF<S"gL-k'1><[xh!j}<&u}W"}qPmxKokZgfQzpYF!z:sazN/VICsv:1_}<q#wGlrC|7,,"jXZE&FF4L+[zy:A_XAMamBMBd\,I92yc8O4HLG3AxUxqRDy(k3.$2ESz&Y@ABj9om*cl}mYED-Ak`iN+!b],ey]V`{A6 d%@+x/ax4OQ-k+7/IL)f|~1<I,2@63GiB LM$~>c/C*(AlnxYdAMOe~W*Bo<FrVX+7l^F<~5f]"#wC b($xfW<yc9L!g=Z,$sG_n}eQ0#@4jaAb]Wd(Qqbe6'G:@BJ)D@IUJHYS8my`CUKgp8qH`:T-~bLKACS$oDFG#iZ;S=|8-;gVU*LQG+yA&oxBzWb*u#|3|bz!.}+cT[@~nTES+DKhV-q9jb@ +x"&(1eH=W(A}Cm1:zX{&'5xhe(7FtGT7ML10b{_x]>#r4NTn|]3Ky~OQeky]smUrQs}oM,|,c>L!YkJH@Mh#/t+blu?
""".strip()

ALLOWED_UNITS = (1, 2)

VERSION = 1

