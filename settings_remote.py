from settings_shared import *

PYRO_HOST_NAME = "0.0.0.0"

PYRO_PORT = 3500

INSTANCE = "home"

ALLOWED_UNITS = (1, 2, 3, 4)