#!/usr/bin/env python3

import logging
import Pyro4
from bdchecker.db_utils import create_database
from bdchecker.unit_util import Executor

from bdchecker.database import schema, functions
from bdchecker.unit_util import get_results_gross, get_results

from argparse import ArgumentParser
import getpass
import importlib

parser = ArgumentParser()
subparsers = parser.add_subparsers()
parser.add_argument("--settings", default="settings")

create_tables = subparsers.add_parser("create_tables")
create_tables.set_defaults(action="create_tables")

set_password = subparsers.add_parser("set_password")
set_password.set_defaults(action="set_password")
set_password.add_argument("--id", help="Twój numer indeksu", required=True)

createdb = subparsers.add_parser("createdb")
createdb.set_defaults(action="createdb")
createdb.add_argument("--zaj", help="Numer zajęć")

serve_pyro = subparsers.add_parser("serve_pyro")
serve_pyro.set_defaults(action="serve_pyro")

show_marks = subparsers.add_parser("show_marks")
show_marks.set_defaults(action="show_marks")
show_marks.add_argument("--details", help="Pokaż oceny z zadań", action="store_true")
show_marks.add_argument("--id", help="Twój numer indeksu", required=True)



if __name__ == "__main__":
    args = parser.parse_args()

    settings = importlib.import_module(args.settings)

    if args.action == "create_tables":
        create_database("db_oceny")
        schema.Base.metadata.create_all(settings.ENGINE)
    elif args.action == "set_password":
        print("Podaj hasło, nie powinno to być żadne WAŻNE hasło, będzie "
              "przechowywane w tekście jawnym.")
        password = getpass.getpass("Nowe hasło:")
        super_password = getpass.getpass("Hasło prowadzącego")
        functions.set_password(args.id, super_password, password)
    elif args.action == "createdb":
        if args.zaj == "zaj1":
            from units.unit1.create_unit import create_unit
            create_unit()
        elif args.zaj == "zaj2":
            from units.unit2.create_unit import create_unit
            create_unit()
        elif args.zaj == "zaj3":
            from units.unit3.util import set_up_engine
            set_up_engine(settings.ZAJ_3_TEST_ENGINE)
        else:
            raise ValueError()
    elif args.action == "serve_pyro":
        # Pyro4.config.PYRO_LOGLEVEL = logging.DEBUG
        Pyro4.config.HMAC_KEY = settings.HMAC_KEY
        # Pyro4.config.COMPRESSION = True
        # Pyro4.config.SERVERTYPE = "multiplex"
        Pyro4.Daemon.serveSimple(
            port=settings.PYRO_PORT,
            host=settings.PYRO_HOST_NAME,
            ns=False, objects={
                Executor(settings): "executor"
            }, )
    elif args.action == "show_marks":
        if args.details:
            results = get_results(settings, args.id)
        else:
            results = get_results_gross(settings, args.id)
        for row in results:
            print(row)



