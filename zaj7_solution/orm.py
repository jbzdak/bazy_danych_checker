from sqlalchemy.ext.declarative import declarative_base

__author__ = 'jb'

from sqlalchemy import create_engine, Column, String

Base = declarative_base()


class Tag(Base):
    __tablename__ = "TAG"
    key = Column(String(), primary_key=True)
    label = Column(String())

    def __init__(self, key, label):
        self.key = key
        self.label = label

    def __eq__(self, other):
        return self.key == other.key and self.label == other.label

    def __repr__(self):
        return repr(u"<Tag {0.key}:{0.label}>".format(self))
