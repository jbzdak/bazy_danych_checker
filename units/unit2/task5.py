from bdchecker.api import QueryChecker
from units.unit2.util import Zaj2TaskChecker

__author__ = 'jb'


class TaskChecker(Zaj2TaskChecker):

    task_no = 5

    class TestSuite(QueryChecker):
        test_columns = False
        expected_query = """
SELECT
	data_source,
	COUNT(*) AS count
FROM (SELECT DISTINCT data_source, point_type FROM "DATA_POINT_DAILY" ORDER BY data_source, point_type) as foo
GROUP BY data_source
ORDER BY data_source
            """



