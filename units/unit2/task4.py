from bdchecker.api import QueryChecker
from units.unit2.util import Zaj2TaskChecker

__author__ = 'jb'


class TaskChecker(Zaj2TaskChecker):

    task_no = 4

    class TestSuite(QueryChecker):
        join =  False
        subselect_count = 0
        expected_query = """
SELECT ws.date, ws.value * cos(radians(wd.value)) as wind_x,  ws.value * SIN(radians(wd.value)) as wind_y
FROM "DATA_POINT_DAILY" ws, "DATA_POINT_DAILY" wd
WHERE ws.point_type = 6 AND wd.point_type = 7 AND wd.data_source = ws.data_source AND wd.date = ws.date
ORDER BY ws.data_source, ws.date
            """
