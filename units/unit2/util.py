from bdchecker.api import DatabaseTaskChecker
from settings import ZAJ_2_ENGINE


class Zaj2TaskChecker(DatabaseTaskChecker):

    unit_no = 2
    engine = ZAJ_2_ENGINE