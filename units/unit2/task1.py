from bdchecker.api import QueryChecker
from units.unit2.util import Zaj2TaskChecker

__author__ = 'jb'


class TaskChecker(Zaj2TaskChecker):

    task_no = 1

    class TestSuite(QueryChecker):
        test_columns = False
        expected_query = """
            SELECT  (SELECT name FROM "DATA_SOURCE" where id = data_source) FROM "DATA_POINT_DAILY" dp WHERE dp.point_type = 0 AND dp.value = (SELECT MAX(dp.value) FROM "DATA_POINT_DAILY" dp WHERE dp.point_type = 0);
        """

        def test_has_subselect(self):
            user_query = self.kwargs['query']

            self.assertGreater(user_query.lower().count("select"), 1,
                               "Nie użyto subselecta")

