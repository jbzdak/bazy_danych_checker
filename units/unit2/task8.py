from bdchecker.api import QueryChecker
from units.unit2.util import Zaj2TaskChecker

__author__ = 'jb'

class TaskChecker(Zaj2TaskChecker):

    task_no = 8

    class TestSuite(QueryChecker):
        test_columns = False
        subselect_count = 0
        distinct = True
        join = False
        expected_query = """
SELECT DISTINCT data_source, point_type FROM "DATA_POINT_DAILY"
ORDER BY data_source, point_type
        """

