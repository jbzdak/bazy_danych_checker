__author__ = 'jb'

import os

DIR = os.path.abspath(os.path.split(__file__)[0])


def create_unit(database_name="zaj1"):
    from bdchecker.db_utils import create_database, load_script, drop_database
    try:
        drop_database(database_name)
    except Exception as e:
        pass
    create_database(database_name)
    load_script(os.path.join(DIR, "zaj2-.sql"), database_name)