from bdchecker.api import QueryChecker
from units.unit2.util import Zaj2TaskChecker

__author__ = 'jb'

class TaskChecker(Zaj2TaskChecker):

    task_no = 2

    class TestSuite(QueryChecker):
        test_columns = False
        subselect_count = 1
        distinct = False
        expected_query = """
SELECT ds.name, pt.name FROM "DATA_SOURCE" ds,  "POINT_TYPE" pt
WHERE EXISTS (SELECT * FROM "DATA_POINT_DAILY" dp WHERE dp.data_source = ds.id AND dp.point_type = pt.id)
ORDER BY ds.name, pt.name
        """

