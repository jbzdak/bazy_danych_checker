from bdchecker.api import QueryChecker
from units.unit2.util import Zaj2TaskChecker

__author__ = 'jb'


class TaskChecker(Zaj2TaskChecker):

    task_no = 7

    class TestSuite(QueryChecker):
        join = True
        subselect_count = 0
        test_columns = False
        expected_query = """
SELECT ds.name as data_source_name , COUNT(*) AS days FROM "DATA_POINT_DAILY"
JOIN "DATA_SOURCE" as ds ON data_source = id
WHERE point_type = 4 AND value > 50 AND date_trunc('year', date) = '2004-01-01'
GROUP BY ds.name
HAVING COUNT(*) > 40
ORDER BY data_source_name

            """