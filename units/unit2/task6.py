from bdchecker.api import QueryChecker
from units.unit2.util import Zaj2TaskChecker

__author__ = 'jb'


class TaskChecker(Zaj2TaskChecker):

    task_no = 6

    class TestSuite(QueryChecker):
        test_columns = False
        join = False
        subselect_count = 1
        expected_query = """
SELECT (SELECT name FROM "DATA_SOURCE" WHERE id = data_source) as data_source_name , COUNT(*) AS days FROM "DATA_POINT_DAILY"
WHERE point_type = 4 AND value > 50 AND date_trunc('year', date) = '2004-01-01'
GROUP BY data_source
HAVING COUNT(*) > 40
ORDER BY data_source_name
            """

