from bdchecker.fs_utils import read_file

__author__ = 'jb'

import unittest
from bdchecker.api import BaseTaskChecker


class Zad1Checker(unittest.TestCase):



    def test_file_contents(self):
        reclieved = self.kwargs['so2']
        correct = read_file(__file__, "task2.csv")
        self.assertEqual(reclieved, correct)


class TaskChecker(BaseTaskChecker):

    TestSuite = Zad1Checker
    unit_no = 1
    task_no = 2

    display_failure_cause = False