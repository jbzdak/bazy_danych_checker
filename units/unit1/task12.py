from .util import Zaj1TaskChecker
from bdchecker.api import QueryChecker, EqualityChecker


class TaskChecker(Zaj1TaskChecker):

    class TestSuite(EqualityChecker):
        expected_args = None
        expected_kwargs = {
            "days": "89"
        }

    task_no = 12

    display_failure_cause = False

