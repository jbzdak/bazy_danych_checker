__author__ = 'jb'


from .util import Zaj1TaskChecker
from bdchecker.api import QueryChecker


class Zad3(QueryChecker):
    expected_query = """
        SELECT wind_speed * cos(radians(wind_dir)) as wind_x,  wind_speed * sin(radians(wind_dir)) as wind_y  from zaj1 ORDER BY date;
    """


class TaskChecker(Zaj1TaskChecker):

    TestSuite = Zad3
    task_no = 3
