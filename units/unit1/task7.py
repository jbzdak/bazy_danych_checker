from .util import Zaj1TaskChecker
from bdchecker.api import QueryChecker


class TaskChecker(Zaj1TaskChecker):

    class TestSuite(QueryChecker):
        expected_query = """
            SELECT AVG(wind_speed) FROM zaj1 where date between date '2012-03-01' and date '2012-03-31'
        """

    task_no = 7

