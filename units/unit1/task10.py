from .util import Zaj1TaskChecker
from bdchecker.api import QueryChecker


class TaskChecker(Zaj1TaskChecker):

    class TestSuite(QueryChecker):
        expected_query = """
            SELECT date_trunc('day', date), AVG(pm_10) FROM zaj1 GROUP BY date_trunc('day', date) order by date_trunc('day', date);
        """

    task_no = 10

