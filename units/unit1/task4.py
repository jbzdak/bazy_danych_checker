__author__ = 'jb'


from .util import Zaj1TaskChecker
from bdchecker.api import QueryChecker


class TaskChecker(Zaj1TaskChecker):

    class TestSuite(QueryChecker):
        expected_query = """
            SELECT pm_10 from zaj1 where wind_speed > 1 AND (ozon = 0 OR ozon is null) ORDER BY date;
        """

    task_no = 4