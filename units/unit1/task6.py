from .util import Zaj1TaskChecker
from bdchecker.api import QueryChecker


class TaskChecker(Zaj1TaskChecker):

    class TestSuite(QueryChecker):
        expected_query = """
            SELECT AVG(wind_speed) FROM zaj1;
        """

    task_no = 6
