from .util import Zaj1TaskChecker
from bdchecker.api import QueryChecker, EqualityChecker


class TaskChecker(Zaj1TaskChecker):

    class TestSuite(EqualityChecker):
        expected_args = None
        expected_kwargs = {
            "max_avg": "79.59"
        }

    task_no = 11

    display_failure_cause = False

