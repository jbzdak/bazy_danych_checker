from .util import Zaj1TaskChecker
from bdchecker.api import QueryChecker, BaseTaskChecker, EqualityChecker


class TaskChecker(Zaj1TaskChecker):

    class TestSuite(EqualityChecker):
        expected_args = None
        expected_kwargs = {
            "exceeded_wind_speed": "1.20",
            "not_exceeded_wind_speed": "1.58"
        }

    task_no = 8

    display_failure_cause = False

