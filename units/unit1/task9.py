from .util import Zaj1TaskChecker
from bdchecker.api import QueryChecker


class TaskChecker(Zaj1TaskChecker):

    class TestSuite(QueryChecker):
        expected_query = """
            SELECT AVG(wind_speed), przekroczenie FROM zaj1 GROUP BY przekroczenie order by przekroczenie;
        """

    task_no = 9

