﻿ALTER TABLE "PRACA_DYPLOMOWA" DROP CONSTRAINT IF EXISTS "PRACA_DYPLOMOWA_promotor_id_fkey";
ALTER TABLE "PRACA_DYPLOMOWA" DROP CONSTRAINT IF EXISTS "PRACA_DYPLOMOWA_student_id_fkey";
ALTER TABLE "PRACA_DYPLOMOWA" DROP CONSTRAINT IF EXISTS "PRACA_DYPLOMOWA_pkey";

ALTER TABLE "PRACOWNIK" DROP CONSTRAINT "prac_gender_is_ok"; 
ALTER TABLE "STUDENT" DROP CONSTRAINT "stud_gender_is_ok"; 
ALTER TABLE "PRACOWNIK" DROP CONSTRAINT "tel_no_is_ok"; 

ALTER TABLE "PRACOWNIK" RENAME TO "PRAC_OLD";
ALTER TABLE "STUDENT" RENAME TO "STUD_OLD";

CREATE TABLE "OSOBA"
(
  id serial NOT NULL PRIMARY KEY,  
  name character varying NOT NULL,
  surname character varying NOT NULL,
  gender smallint NOT NULL,
  CONSTRAINT gender_is_ok CHECK (gender = ANY (ARRAY[0, 1]))
);

CREATE TABLE "STUDENT"
(
  student_id integer,  
  status character varying,    
  message character varying NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT gender_is_ok CHECK (gender = ANY (ARRAY[0, 1])),
  FOREIGN KEY (status) REFERENCES "TAG"("key")
) INHERITS ("OSOBA");

CREATE TABLE "PRACOWNIK"
(   
  pracownik_id integer,
  tel_no character varying,  
  CONSTRAINT tel_no_is_ok CHECK (tel_no::text ~ similar_escape('22\s+234\-\d\d\-\d\d'::text, NULL::text)),
  PRIMARY KEY (id),
  CONSTRAINT gender_is_ok CHECK (gender = ANY (ARRAY[0, 1]))
) INHERITS ("OSOBA");

INSERT INTO "STUDENT"(student_id, name, surname, gender, message,  status) (SELECT id, name, surname, gender, message, status FROM "STUD_OLD");

INSERT INTO "PRACOWNIK"(pracownik_id, name, surname, gender, tel_no) (SELECT id, name, surname, gender, tel_no FROM "PRAC_OLD");

UPDATE "PRACA_DYPLOMOWA" AS pd SET student_id =  s.id FROM "STUDENT" as s WHERE pd.student_id = s.student_id;

UPDATE "PRACA_DYPLOMOWA" SET promotor_id =  s.id FROM "PRACOWNIK" as s WHERE promotor_id = s.pracownik_id;

ALTER TABLE "PRACA_DYPLOMOWA" ADD FOREIGN KEY (student_id) REFERENCES "STUDENT" (id); 
ALTER TABLE "PRACA_DYPLOMOWA" ADD FOREIGN KEY (promotor_id) REFERENCES "PRACOWNIK" (id); 
ALTER TABLE "PRACA_DYPLOMOWA" ADD PRIMARY KEY (student_id, type);

ALTER TABLE "STUDENT" DROP COLUMN student_id;
ALTER TABLE "PRACOWNIK" DROP COLUMN pracownik_id;

DROP TABLE "PRAC_OLD";
DROP TABLE "STUD_OLD";

SELECT * FROM "OSOBA"; 

SELECT * FROM "PRACA_DYPLOMOWA" p;
