﻿ALTER TABLE "PRACA_DYPLOMOWA" DROP CONSTRAINT IF EXISTS "PRACA_DYPLOMOWA_promotor_id_fkey";
ALTER TABLE "PRACA_DYPLOMOWA" DROP CONSTRAINT IF EXISTS "PRACA_DYPLOMOWA_student_id_fkey";
ALTER TABLE "PRACA_DYPLOMOWA" DROP CONSTRAINT IF EXISTS "PRACA_DYPLOMOWA_pkey";

ALTER TABLE "PRACOWNIK" DROP CONSTRAINT "prac_gender_is_ok"; 
ALTER TABLE "STUDENT" DROP CONSTRAINT "stud_gender_is_ok"; 
ALTER TABLE "PRACOWNIK" DROP CONSTRAINT "tel_no_is_ok"; 

INSERT INTO "TAG"(key, label) VALUES ('type:prac', 'pracownik'), ('type:stud', 'student'); 

CREATE TABLE "OSOBA"
(
  id serial NOT NULL PRIMARY KEY,
  student_id integer, 
  pracownik_id integer, 
  type character varying,
  name character varying NOT NULL,
  surname character varying NOT NULL,
  gender smallint NOT NULL,    
  status character varying,    
  message character varying,
  tel_no character varying,
  CONSTRAINT gender_is_ok CHECK (gender = ANY (ARRAY[0, 1])),
  CONSTRAINT "STUDENT_status_fkey" FOREIGN KEY (status)
      REFERENCES "TAG" (key) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tel_no_is_ok CHECK (tel_no::text ~ similar_escape('22\s+234\-\d\d\-\d\d'::text, NULL::text)),
  FOREIGN KEY (type) REFERENCES "TAG"(key),
  CHECK (type LIKE 'type:%'), 
  CHECK(tel_no is not NULL OR type <> 'type:prac'),
  CHECK(message is not NULL OR type <> 'type:stud'),
  CHECK(status is not NULL OR type <> 'type:stud')
);

INSERT INTO "OSOBA"(student_id, type, name, surname, gender, status, message)  (SELECT id, 'type:stud', name, surname, gender, status, message FROM "STUDENT");
INSERT INTO "OSOBA"(pracownik_id, type, name, surname, gender, tel_no)  (SELECT id, 'type:prac', name, surname, gender, tel_no FROM "PRACOWNIK");

UPDATE "PRACA_DYPLOMOWA" AS pd SET student_id = os.id FROM "OSOBA" os WHERE pd.student_id = os.student_id;
UPDATE "PRACA_DYPLOMOWA" AS pd SET promotor_id = os.id FROM "OSOBA" os WHERE pd.promotor_id = os.pracownik_id;

ALTER TABLE "PRACA_DYPLOMOWA" ADD PRIMARY KEY (student_id, type);

ALTER TABLE "OSOBA" DROP COLUMN student_id;
ALTER TABLE "OSOBA" DROP COLUMN pracownik_id;

DROP TABLE "PRACOWNIK";
DROP TABLE "STUDENT";

--SELECT * FROM "OSOBA";