﻿ALTER TABLE "PRACA_DYPLOMOWA" DROP CONSTRAINT IF EXISTS "PRACA_DYPLOMOWA_promotor_id_fkey";
ALTER TABLE "PRACA_DYPLOMOWA" DROP CONSTRAINT IF EXISTS "PRACA_DYPLOMOWA_student_id_fkey";
ALTER TABLE "PRACA_DYPLOMOWA" DROP CONSTRAINT IF EXISTS "PRACA_DYPLOMOWA_pkey";

ALTER TABLE "PRACOWNIK" DROP CONSTRAINT "prac_gender_is_ok"; 
ALTER TABLE "STUDENT" DROP CONSTRAINT "stud_gender_is_ok"; 
ALTER TABLE "PRACOWNIK" DROP CONSTRAINT "tel_no_is_ok"; 

ALTER TABLE "PRACOWNIK" RENAME TO "PRAC_OLD";
ALTER TABLE "STUDENT" RENAME TO "STUD_OLD";

CREATE TABLE "OSOBA"
(
  id serial NOT NULL PRIMARY KEY,
  student_id integer, 
  pracownik_id integer,
  name character varying NOT NULL,
  surname character varying NOT NULL,
  gender smallint NOT NULL,
  CONSTRAINT gender_is_ok CHECK (gender = ANY (ARRAY[0, 1]))
);

CREATE TABLE "STUDENT"
(
   id integer NOT NULL,     
   
    status character varying,    
  message character varying NOT NULL,
  PRIMARY KEY (id), 
    FOREIGN KEY (id) REFERENCES "OSOBA" (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT "STUDENT_status_fkey" FOREIGN KEY (status)
      REFERENCES "TAG" (key) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
    
    
);

CREATE TABLE "PRACOWNIK"
(
   id integer NOT NULL,     
	tel_no character varying,
  PRIMARY KEY (id), 
  FOREIGN KEY (id) REFERENCES "OSOBA" (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tel_no_is_ok CHECK (tel_no::text ~ similar_escape('22\s+234\-\d\d\-\d\d'::text, NULL::text))    
);

INSERT INTO "OSOBA"(student_id, name, surname, gender) (SELECT id, name, surname, gender FROM "STUD_OLD");

INSERT INTO "STUDENT"(id, message,  status) (SELECT os.id, old.message, old.status FROM "STUD_OLD" old, "OSOBA" os WHERE old.id = os.student_id);

INSERT INTO "OSOBA"(pracownik_id, name, surname, gender) (SELECT id, name, surname, gender FROM "PRAC_OLD");

INSERT INTO "PRACOWNIK"(id, tel_no) (SELECT os.id, old.tel_no FROM "PRAC_OLD" old, "OSOBA" os WHERE old.id = os.pracownik_id);

UPDATE "PRACA_DYPLOMOWA" AS pd SET student_id =  o.id FROM "OSOBA" as o, "STUDENT" as s WHERE pd.student_id = o.student_id AND o.id=s.id; 

UPDATE "PRACA_DYPLOMOWA" SET promotor_id =  o.id FROM "OSOBA" as o, "PRACOWNIK" as s WHERE promotor_id = o.pracownik_id AND o.id=s.id; 

ALTER TABLE "PRACA_DYPLOMOWA" ADD FOREIGN KEY (student_id) REFERENCES "STUDENT" (id); 
ALTER TABLE "PRACA_DYPLOMOWA" ADD FOREIGN KEY (promotor_id) REFERENCES "PRACOWNIK" (id); 
ALTER TABLE "PRACA_DYPLOMOWA" ADD PRIMARY KEY (student_id, type);

ALTER TABLE "OSOBA" DROP COLUMN student_id;
ALTER TABLE "OSOBA" DROP COLUMN pracownik_id;

SELECT * FROM "OSOBA"; 

SELECT * FROM "PRACA_DYPLOMOWA" p;
